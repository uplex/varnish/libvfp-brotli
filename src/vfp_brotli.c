/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Implementation inspired by Varnish cache_gzip.c
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <brotli/encode.h>
#include <brotli/decode.h>

#include "cache/cache.h"
#include "cache/cache_filter.h"
#include "vcl.h"

#include "vcc_if.h"
#include "VSC_brotli.h"

#define VFAIL(ctx, fmt, ...) \
        VRT_fail((ctx), "vfp brotli failure: " fmt, __VA_ARGS__)

/* cf. varnishd default gzip_buffer */
#define DEFAULT_BUFSZ (32 * 1024)

struct vbr_stream {
	const uint8_t		*next_in;
	uint8_t			*next_out;
	size_t			avail_in;
	size_t			avail_out;
	size_t			total_out;
};

enum vbr_which {ENC, DEC};

struct vbr {
	unsigned		magic;
#define VBR_MAGIC		0x467c387b
	struct vbr_stream	stream;
	union {
		BrotliEncoderState *enc;
		BrotliDecoderState *dec;
	}			state;
	uint8_t			*buf;
	ssize_t			bufsz;
	ssize_t			buflen;
	enum vbr_which		which;
};

struct vbr_settings {
	unsigned		magic;
#define VBR_SETTINGS_MAGIC	0xa61992aa
	VCL_BYTES		bufsz;
	uint32_t		quality;
	uint32_t		large_win;
	uint32_t		lgwin;
	enum BrotliEncoderMode	mode;
	enum vbr_which		which;
};

struct vmod_brotli_encoder {
	unsigned	magic;
#define VMOD_BROTLI_ENCODER_MAGIC 0x1490a42a
	char		*vcl_name;
	struct vfp	*vfp;
};

struct vmod_brotli_decoder {
	unsigned	magic;
#define VMOD_BROTLI_DECODER_MAGIC 0x263b6d01
	char		*vcl_name;
	struct vfp	*vfp;
};

struct custom_vfp_entry {
	unsigned	magic;
#define CUSTOM_VFP_MAGIC 0xfc88cb98
	VSLIST_ENTRY(custom_vfp_entry) list;
	struct vfp	*vfp;
};

VSLIST_HEAD(custom_vfp_head, custom_vfp_entry);

struct vfp_priv {
	unsigned		magic;
#define VFP_PRIV_MAGIC		0xc79b73f7
	struct vbr_settings	*settings;
	struct VSC_brotli	*stats;
	struct vsc_seg		*vsc_seg;
};

static struct vbr *
newVBR(enum vbr_which which)
{
	struct vbr *vbr;

	ALLOC_OBJ(vbr, VBR_MAGIC);
	if (vbr == NULL)
		return (NULL);
	if (which == ENC) {
		vbr->state.enc = BrotliEncoderCreateInstance(NULL, NULL, NULL);
		if (vbr->state.enc == NULL)
			return (NULL);
	}
	else {
		vbr->state.dec = BrotliDecoderCreateInstance(NULL, NULL, NULL);
		if (vbr->state.dec == NULL)
			return (NULL);
	}
	vbr->which = which;
	return (vbr);
}

static void
destroy(struct vbr **vp)
{
	struct vbr *vbr;

	TAKE_OBJ_NOTNULL(vbr, vp, VBR_MAGIC);
	if (vbr->which == ENC)
		BrotliEncoderDestroyInstance(vbr->state.enc);
	else
		BrotliDecoderDestroyInstance(vbr->state.dec);
	if (vbr->buf != NULL)
		free(vbr->buf);
	FREE_OBJ(vbr);
}

static void
setEncoderParams(struct vbr *vbr, const struct vbr_settings *settings)
{
	CHECK_OBJ_NOTNULL(settings, VBR_SETTINGS_MAGIC);
	assert(BrotliEncoderSetParameter(vbr->state.enc, BROTLI_PARAM_QUALITY,
					 settings->quality) == BROTLI_TRUE);
	assert(BrotliEncoderSetParameter(vbr->state.enc,
					 BROTLI_PARAM_LARGE_WINDOW,
					 settings->large_win) == BROTLI_TRUE);
	assert(BrotliEncoderSetParameter(vbr->state.enc, BROTLI_PARAM_LGWIN,
					 settings->lgwin) == BROTLI_TRUE);
	assert(BrotliEncoderSetParameter(vbr->state.enc, BROTLI_PARAM_MODE,
					 settings->mode) == BROTLI_TRUE);
}

static void
setDecoderParams(struct vbr *vbr, const struct vbr_settings *settings)
{
	CHECK_OBJ_NOTNULL(settings, VBR_SETTINGS_MAGIC);
	assert(BrotliDecoderSetParameter(vbr->state.dec,
					 BROTLI_DECODER_PARAM_LARGE_WINDOW,
					 settings->large_win) == BROTLI_TRUE);
}

static inline void
setParams(struct vbr *vbr, const struct vbr_settings *settings)
{
	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);
	if (vbr->which == ENC)
		setEncoderParams(vbr, settings);
	else
		setDecoderParams(vbr, settings);
}

static int
getBuf(struct vbr *vbr, const struct vbr_settings *settings)
{
	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);
	CHECK_OBJ_NOTNULL(settings, VBR_SETTINGS_MAGIC);
	AZ(vbr->bufsz);
	AZ(vbr->buflen);
	AZ(vbr->buf);

	vbr->bufsz = settings->bufsz;
	vbr->buf = malloc(vbr->bufsz);
	if (vbr->buf == NULL) {
		vbr->bufsz = 0;
		return (-1);
	}
	return (0);
}

static const char *
decodeErrMsg(struct vbr *vbr)
{
	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);
	assert(vbr->which == DEC);

	return (BrotliDecoderErrorString(
			BrotliDecoderGetErrorCode(vbr->state.dec)));
}

static void
setInputBuf(struct vbr *vbr, const void *ptr, ssize_t len)
{
	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);

	AZ(vbr->stream.avail_in);
	vbr->stream.next_in = ptr;
	vbr->stream.avail_in = len;
}

static int
isInputBufEmpty(const struct vbr *vbr)
{
	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);
	return (vbr->stream.avail_in == 0);
}

static void
setOutputBuf(struct vbr *vbr, const void *ptr, ssize_t len)
{
	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);

	vbr->stream.next_out = TRUST_ME(ptr);
	vbr->stream.avail_out = len;
}

static BROTLI_BOOL
encode(struct vbr *vbr, ssize_t *dl, int finished, struct VSC_brotli *stats)
{
	BROTLI_BOOL ret;
	const uint8_t *before;
	size_t inb4;
	enum BrotliEncoderOperation op = BROTLI_OPERATION_PROCESS;

	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);
	assert(vbr->which == ENC);
	before = vbr->stream.next_out;
	inb4 = vbr->stream.avail_in;

	if (finished)
		op = BROTLI_OPERATION_FINISH;
	ret = BrotliEncoderCompressStream(vbr->state.enc, op,
					  &vbr->stream.avail_in,
					  &vbr->stream.next_in,
					  &vbr->stream.avail_out,
					  &vbr->stream.next_out,
					  &vbr->stream.total_out);
	*dl = (const uint8_t *)vbr->stream.next_out - before;
	if (stats != NULL) {
		assert(inb4 >= vbr->stream.avail_in);
		stats->in += (inb4 - vbr->stream.avail_in);
	}
	return (ret);
}

static BrotliDecoderResult
decode(struct vbr *vbr, ssize_t *dl, struct VSC_brotli *stats)
{
	BrotliDecoderResult ret;
	const uint8_t *before;
	size_t inb4;

	CHECK_OBJ_NOTNULL(vbr, VBR_MAGIC);
	assert(vbr->which == DEC);
	before = vbr->stream.next_out;
	inb4 = vbr->stream.avail_in;

	ret = BrotliDecoderDecompressStream(vbr->state.dec,
					  &vbr->stream.avail_in,
					  &vbr->stream.next_in,
					  &vbr->stream.avail_out,
					  &vbr->stream.next_out,
					  &vbr->stream.total_out);
	*dl = (const uint8_t *)vbr->stream.next_out - before;
	if (stats != NULL) {
		assert(inb4 >= vbr->stream.avail_in);
		stats->in += (inb4 - vbr->stream.avail_in);
	}
	return (ret);
}

/* VFP interfaces */

static const struct vfp vfp_br, vfp_unbr;

/* init and fini methods for both br and unbr */

static enum vfp_status v_matchproto_(vfp_init_f)
vfp_br_init(VRT_CTX, struct vfp_ctx *vc, struct vfp_entry *ent)
{
	struct vbr *vbr;
	const struct vfp_priv *priv;
	const struct vbr_settings *settings;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vc, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(priv, ent->vfp->priv1, VFP_PRIV_MAGIC);
	CHECK_OBJ_NOTNULL(priv->settings, VBR_SETTINGS_MAGIC);
	settings = priv->settings;

	/*
	 * Ignore partial responses to range requests (as Varnish does for
	 * gzip and gunzip).
	 */
	if (http_GetStatus(vc->resp) == 206)
		return (VFP_NULL);

	if (priv->stats != NULL)
		priv->stats->ops++;

	if (settings->which == ENC) {
		if (http_GetHdr(vc->resp, H_Content_Encoding, NULL))
			return (VFP_NULL);
		vbr = newVBR(ENC);
	}
	else {
		if (!http_HdrIs(vc->resp, H_Content_Encoding, "br"))
			return (VFP_NULL);
		vbr = newVBR(DEC);
	}
	if (vbr == NULL)
		return (VFP_ERROR);
	ent->priv1 = vbr;
	if (getBuf(vbr, settings))
		return (VFP_ERROR);
	setParams(vbr, settings);
	setInputBuf(vbr, vbr->buf, 0);
	AZ(vbr->buflen);

	http_Unset(vc->resp, H_Content_Encoding);
	http_Unset(vc->resp, H_Content_Length);
	RFC2616_Weaken_Etag(vc->resp);
	vc->obj_flags |= OF_CHGCE;

	if (settings->which == ENC) {
		http_SetHeader(vc->resp, "Content-Encoding: br");
		RFC2616_Vary_AE(vc->resp);
	}

	return (VFP_OK);
}

static void v_matchproto_(vfp_fini_f)
vfp_br_fini(struct vfp_ctx *ctx, struct vfp_entry *ent)
{
	struct vbr *vbr;
	const struct vfp_priv *priv;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(priv, ent->vfp->priv1, VFP_PRIV_MAGIC);

	if (ent->priv1 != NULL) {
		CAST_OBJ(vbr, ent->priv1, VBR_MAGIC);
		if (priv->stats != NULL)
			priv->stats->out += vbr->stream.total_out;
		ent->priv1 = NULL;
		destroy(&vbr);
	}
}

/* pull for br (compression) */

static enum vfp_status v_matchproto_(vfp_pull_f)
vfp_br_pull(struct vfp_ctx *ctx, struct vfp_entry *ent, void *ptr,
	    ssize_t *lenp)
{
	struct vbr *vbr;
	ssize_t len, l, dl;
	enum vfp_status vp = VFP_ERROR;
	int finished = 0;
	BROTLI_BOOL done;
	const struct vfp_priv *priv;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	CAST_OBJ_NOTNULL(vbr, ent->priv1, VBR_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(priv, ent->vfp->priv1, VFP_PRIV_MAGIC);
	assert(vbr->which == ENC);
	AN(ptr);
	AN(lenp);

	len = *lenp;
	*lenp = 0;
	setOutputBuf(vbr, ptr, len);

	do {
		if (isInputBufEmpty(vbr)) {
			len = 0;
			do {
				l = vbr->bufsz - len;
				vp = VFP_Suck(ctx, vbr->buf + len, &l);
				len += l;
			} while (vp == VFP_OK && len < vbr->bufsz);
			if (vp == VFP_ERROR)
				break;
			if (vp == VFP_END)
				finished = 1;
			setInputBuf(vbr, vbr->buf, len);
		}
		if (!isInputBufEmpty(vbr) || finished) {
			done = encode(vbr, &dl, finished, priv->stats);
			if (done != BROTLI_TRUE)
				return (VFP_Error(ctx, "brotli encode failed"));
			if (dl > 0) {
				*lenp = dl;
				return (VFP_OK);
			}
		}
		AN(isInputBufEmpty(vbr));
	} while (!finished);

	if (done != BROTLI_TRUE)
		return (VFP_Error(ctx, "brotli encode failed"));
	return (VFP_END);
}

/* pull for unbr (decompression) */

static enum vfp_status v_matchproto_(vfp_pull_f)
vfp_unbr_pull(struct vfp_ctx *ctx, struct vfp_entry *ent, void *ptr,
	      ssize_t *lenp)
{
	struct vbr *vbr;
	ssize_t len, l, dl;
	enum vfp_status vp = VFP_ERROR;
	BrotliDecoderResult result = BROTLI_DECODER_RESULT_ERROR;
	const struct vfp_priv *priv;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	CAST_OBJ_NOTNULL(vbr, ent->priv1, VBR_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(priv, ent->vfp->priv1, VFP_PRIV_MAGIC);
	assert(vbr->which == DEC);
	AN(ptr);
	AN(lenp);

	len = *lenp;
	*lenp = 0;
	setOutputBuf(vbr, ptr, len);

	do {
		if (isInputBufEmpty(vbr)) {
			len = 0;
			do {
				l = vbr->bufsz - len;
				vp = VFP_Suck(ctx, vbr->buf + len, &l);
				len += l;
			} while (vp == VFP_OK && len < vbr->bufsz);
			if (vp == VFP_ERROR)
				break;
			setInputBuf(vbr, vbr->buf, len);
		}
		if (!isInputBufEmpty(vbr) || vp == VFP_END) {
			result = decode(vbr, &dl, priv->stats);
			if (result == BROTLI_DECODER_RESULT_SUCCESS &&
			    !isInputBufEmpty(vbr))
				return(VFP_Error(ctx,
						 "Junk after brotli data"));
			if (result == BROTLI_DECODER_RESULT_ERROR)
				return (VFP_Error(ctx,
						  "Invalid brotli data: %s",
						  decodeErrMsg(vbr)));
			if (dl > 0) {
				*lenp = dl;
				return (VFP_OK);
			}
		}
		AN(isInputBufEmpty(vbr));
	} while (result == BROTLI_DECODER_RESULT_NEEDS_MORE_INPUT);

	if (result != BROTLI_DECODER_RESULT_SUCCESS)
		return (VFP_Error(ctx, "Invalid brotli data at end: %s",
				  decodeErrMsg(vbr)));
	return (VFP_END);
}

static struct vbr_settings default_encoder_settings = {
	.magic = VBR_SETTINGS_MAGIC,
	.bufsz = DEFAULT_BUFSZ,
	.quality = BROTLI_DEFAULT_QUALITY,
	.large_win = 0,
	.lgwin = BROTLI_DEFAULT_WINDOW,
	.mode = BROTLI_DEFAULT_MODE,
	.which = ENC,
};

static struct vbr_settings default_decoder_settings = {
	.magic = VBR_SETTINGS_MAGIC,
	.bufsz = DEFAULT_BUFSZ,
	.large_win = 0,
	.which = DEC,
};

static struct vfp_priv default_encoder = {
	.magic = VFP_PRIV_MAGIC,
	.settings = &default_encoder_settings,
	.stats = NULL,
	.vsc_seg = NULL,
};

static struct vfp_priv default_decoder = {
	.magic = VFP_PRIV_MAGIC,
	.settings = &default_decoder_settings,
	.stats = NULL,
	.vsc_seg = NULL,
};

static const struct vfp vfp_br = {
	.name = "br",
	.init = vfp_br_init,
	.pull = vfp_br_pull,
	.fini = vfp_br_fini,
	.priv1 = &default_encoder,
};

static const struct vfp vfp_unbr = {
	.name = "unbr",
	.init = vfp_br_init,
	.pull = vfp_unbr_pull,
	.fini = vfp_br_fini,
	.priv1 = &default_decoder,
};

/* Event function */

static struct custom_vfp_head *
init_priv_vcl(struct vmod_priv *priv)
{
	struct custom_vfp_head *vfph;

	AN(priv);
	if (priv->priv == NULL) {
		vfph = malloc(sizeof(*vfph));
		AN(vfph);
		priv->priv = vfph;
		VSLIST_INIT(vfph);
	}
	else
		vfph = priv->priv;
	return (vfph);
}

int
vmod_event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	struct custom_vfp_head *vfph;
	struct custom_vfp_entry *vfpe;
	struct vfp_priv *vfp_priv;
	const char *err;

	ASSERT_CLI();
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(priv);

	vfph = init_priv_vcl(priv);

	switch(e) {
	case VCL_EVENT_LOAD:
		err = VRT_AddFilter(ctx, &vfp_br, NULL);
		if (err != NULL)
			return (1);
		err = VRT_AddFilter(ctx, &vfp_unbr, NULL);
		if (err != NULL)
			return (1);
		CAST_OBJ_NOTNULL(vfp_priv, TRUST_ME(vfp_br.priv1),
				 VFP_PRIV_MAGIC);
		if (vfp_priv->stats == NULL)
			vfp_priv->stats = VSC_brotli_New(NULL,
							 &vfp_priv->vsc_seg,
							 "br");
		CAST_OBJ_NOTNULL(vfp_priv, TRUST_ME(vfp_unbr.priv1),
				 VFP_PRIV_MAGIC);
		if (vfp_priv->stats == NULL)
			vfp_priv->stats = VSC_brotli_New(NULL,
							 &vfp_priv->vsc_seg,
							 "unbr");
		return (0);
	case VCL_EVENT_DISCARD:
		VRT_RemoveFilter(ctx, &vfp_br, NULL);
		VRT_RemoveFilter(ctx, &vfp_unbr, NULL);
		while (!VSLIST_EMPTY(vfph)) {
			vfpe = VSLIST_FIRST(vfph);
			CHECK_OBJ_NOTNULL(vfpe, CUSTOM_VFP_MAGIC);
			if (vfpe->vfp != NULL) {
				if (vfpe->vfp->priv1 != NULL) {
					CAST_OBJ(vfp_priv,
						 TRUST_ME(vfpe->vfp->priv1),
						 VFP_PRIV_MAGIC);
					if (vfp_priv->settings != NULL) {
						CHECK_OBJ(vfp_priv->settings,
							  VBR_SETTINGS_MAGIC);
						FREE_OBJ(vfp_priv->settings);
					}
					if (vfp_priv->vsc_seg != NULL)
						VSC_brotli_Destroy(
							&vfp_priv->vsc_seg);
					/* XXX free(vfp_priv->stats) ? */
				}
				VRT_RemoveFilter(ctx, vfpe->vfp, NULL);
				free(vfpe->vfp);
			}
			VSLIST_REMOVE_HEAD(vfph, list);
			FREE_OBJ(vfpe);
		}
		free(vfph);
		return (0);
	case VCL_EVENT_WARM:
		VSLIST_FOREACH(vfpe, vfph, list) {
			CHECK_OBJ_NOTNULL(vfpe, CUSTOM_VFP_MAGIC);
			if (vfpe->vfp != NULL && vfpe->vfp->priv1 != NULL) {
				CAST_OBJ(vfp_priv, TRUST_ME(vfpe->vfp->priv1),
					 VFP_PRIV_MAGIC);
				if (vfp_priv->vsc_seg != NULL)
					VRT_VSC_Reveal(vfp_priv->vsc_seg);
			}
		}
		return (0);
	case VCL_EVENT_COLD:
		VSLIST_FOREACH(vfpe, vfph, list) {
			CHECK_OBJ_NOTNULL(vfpe, CUSTOM_VFP_MAGIC);
			if (vfpe->vfp != NULL && vfpe->vfp->priv1 != NULL) {
				CAST_OBJ(vfp_priv, TRUST_ME(vfpe->vfp->priv1),
					 VFP_PRIV_MAGIC);
				if (vfp_priv->vsc_seg != NULL)
					VRT_VSC_Hide(vfp_priv->vsc_seg);
			}
		}
		return (0);
	default:
		WRONG("illegal event enum");
	}
	NEEDLESS(return (0));
}

/* Object encoder */

static int
coder_init(VRT_CTX, const char *vcl_name, struct vmod_priv *priv,
	   VCL_STRING filter_name, VCL_BYTES bufsz, struct vfp **vfpp,
	   struct vbr_settings **settingsp)
{
	struct vfp *vfp;
	struct vfp_priv *vfp_priv;
	struct vbr_settings *settings;
	struct custom_vfp_head *vfph;
	struct custom_vfp_entry *vfpe;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(vcl_name);
	AN(priv);
	AN(filter_name);
	AN(vfpp);
	AN(settingsp);
	assert(bufsz >= 0);

	if (*filter_name == '\0') {
		VFAIL(ctx, "new %s: filter name must be non-empty", vcl_name);
		return (-1);
	}
	if (strcmp(filter_name, "br") == 0 || strcmp(filter_name, "unbr") == 0
	    || strcmp(filter_name, "esi") == 0
	    || strcmp(filter_name, "esi_gzip") == 0
	    || strcmp(filter_name, "gunzip") == 0
	    || strcmp(filter_name, "gzip") == 0
	    || strcmp(filter_name, "testgunzip") == 0) {
		VFAIL(ctx,
		      "new %s: filter name %s already in use by another VFP",
		      vcl_name, filter_name);
		return (-1);
	}
	if (bufsz == 0) {
		VFAIL(ctx, "new %s: buffer size must be > 0b", vcl_name);
		return (-1);
	}

	errno = 0;
	vfp = malloc(sizeof(*vfp));
	if (vfp == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for VFP: %s",
		      vcl_name, strerror(errno));
		return (-1);
	}
	*vfpp = vfp;

	errno = 0;
	ALLOC_OBJ(settings, VBR_SETTINGS_MAGIC);
	if (settings == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for settings: %s",
		      vcl_name, strerror(errno));
		return (-1);
	}
	*settingsp = settings;

	errno = 0;
	ALLOC_OBJ(vfp_priv, VFP_PRIV_MAGIC);
	if (vfp_priv == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for VFP priv obj: %s",
		      vcl_name, strerror(errno));
		return (-1);
	}
	*settingsp = settings;

	errno = 0;
	ALLOC_OBJ(vfpe, CUSTOM_VFP_MAGIC);
	if (vfpe == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for VFP list entry: %s",
		      vcl_name, strerror(errno));
		return (-1);
	}

	settings->bufsz = bufsz;
	vfp_priv->settings = settings;

	vfp->name = strdup(filter_name);
	vfp->init = vfp_br_init;
	vfp->fini = vfp_br_fini;
	vfp->priv1 = vfp_priv;

	vfph = init_priv_vcl(priv);
	vfpe->vfp = vfp;
	VSLIST_INSERT_HEAD(vfph, vfpe, list);
	return (0);
}

static void
create_stats(VRT_CTX, const struct vfp *vfp, const char *vcl_name)
{
	struct vfp_priv *vfp_priv;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(vcl_name);
	assert(ctx->method == VCL_MET_INIT);

	AN(vfp);
	CAST_OBJ_NOTNULL(vfp_priv, TRUST_ME(vfp->priv1), VFP_PRIV_MAGIC);

	vfp_priv->stats = VSC_brotli_New(NULL, &vfp_priv->vsc_seg, "%s.%s",
					 VCL_Name(ctx->vcl), vcl_name);
	AN(vfp_priv->stats);
	memset(vfp_priv->stats, 0, sizeof(*vfp_priv->stats));
}

VCL_VOID
vmod_encoder__init(VRT_CTX, struct vmod_brotli_encoder **encp,
		   const char *vcl_name, struct vmod_priv *priv,
		   VCL_STRING filter_name, VCL_BYTES bufsz, VCL_INT quality,
		   VCL_BOOL large_win, VCL_INT lgwin, VCL_ENUM mode)
{
	struct vmod_brotli_encoder *enc;
	struct vfp *vfp = NULL;
	struct vbr_settings *settings = NULL;

	AN(encp);
	AZ(*encp);
	AN(mode);

	if (quality < BROTLI_MIN_QUALITY || quality > BROTLI_MAX_QUALITY) {
		VFAIL(ctx, "new %s: quality %jd out of range (%d to %d)",
		      vcl_name, (intmax_t)quality, BROTLI_MIN_QUALITY,
		      BROTLI_MAX_QUALITY);
		return;
	}

	if (lgwin < BROTLI_MIN_WINDOW_BITS || lgwin > BROTLI_MAX_WINDOW_BITS) {
		VFAIL(ctx, "new %s: lgwin %jd out of range (%d to %d)",
		      vcl_name, (intmax_t)lgwin, BROTLI_MIN_WINDOW_BITS,
		      BROTLI_MAX_WINDOW_BITS);
		return;
	}

	if (coder_init(ctx, vcl_name, priv, filter_name, bufsz, &vfp,
		       &settings) != 0)
		return;
	AN(vfp);
	CHECK_OBJ_NOTNULL(settings, VBR_SETTINGS_MAGIC);

	errno = 0;
	ALLOC_OBJ(enc, VMOD_BROTLI_ENCODER_MAGIC);
	if (enc == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for object: %s",
		      vcl_name, strerror(errno));
		return;
	}

	vfp->pull = vfp_br_pull;
	if (VRT_AddFilter(ctx, vfp, NULL))
		// XXX minor leak from allocations above
		return;
	settings->which = ENC;
	settings->quality = quality;
	settings->large_win = large_win;
	settings->lgwin = lgwin;
	switch (mode[0]) {
	case 'G':
		assert(mode == enum_vmod_brotli_GENERIC);
		settings->mode = BROTLI_MODE_GENERIC;
		break;
	case 'T':
		assert(mode == enum_vmod_brotli_TEXT);
		settings->mode = BROTLI_MODE_TEXT;
		break;
	case 'F':
		assert(mode == enum_vmod_brotli_FONT);
		settings->mode = BROTLI_MODE_FONT;
		break;
	default:
		WRONG("illegal mode enum");
	}
	enc->vfp = vfp;
	enc->vcl_name = strdup(vcl_name);
	*encp = enc;
}

/*
 * The settings and custom VFP objects and PRIV_VCL list entry are freed
 * on the DISCARD event, because we need a VRT_CTX to call
 * VRT_RemoveVFP().
 */
VCL_VOID
vmod_encoder__fini(struct vmod_brotli_encoder **encp)
{
	struct vmod_brotli_encoder *enc;

	if (encp == NULL || *encp == NULL)
		return;
	CHECK_OBJ(*encp, VMOD_BROTLI_ENCODER_MAGIC);
	enc = *encp;
	*encp = NULL;
	if (enc->vcl_name != NULL)
		free(enc->vcl_name);
	FREE_OBJ(enc);
}

VCL_VOID
vmod_encoder_create_stats(VRT_CTX, struct vmod_brotli_encoder *enc)
{
	CHECK_OBJ_NOTNULL(enc, VMOD_BROTLI_ENCODER_MAGIC);
	create_stats(ctx, enc->vfp, enc->vcl_name);
}

/* Object decoder */

VCL_VOID
vmod_decoder__init(VRT_CTX, struct vmod_brotli_decoder **decp,
		   const char *vcl_name, struct vmod_priv *priv,
		   VCL_STRING filter_name, VCL_BYTES bufsz, VCL_BOOL large_win)
{
	struct vmod_brotli_decoder *dec;
	struct vfp *vfp = NULL;
	struct vbr_settings *settings = NULL;

	AN(decp);
	AZ(*decp);

	if (coder_init(ctx, vcl_name, priv, filter_name, bufsz, &vfp,
		       &settings) != 0)
		return;
	AN(vfp);
	CHECK_OBJ_NOTNULL(settings, VBR_SETTINGS_MAGIC);

	errno = 0;
	ALLOC_OBJ(dec, VMOD_BROTLI_DECODER_MAGIC);
	if (dec == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for object: %s",
		      vcl_name, strerror(errno));
		return;
	}

	vfp->pull = vfp_unbr_pull;
	if (VRT_AddFilter(ctx, vfp, NULL))
		// XXX minor leak from allocations above
		return;
	settings->which = DEC;
	settings->large_win = large_win;
	dec->vfp = vfp;
	dec->vcl_name = strdup(vcl_name);
	*decp = dec;
}

/*
 * As above, free the settings, custom VFP and PRIV_VCL list on DISCARD.
 */
VCL_VOID
vmod_decoder__fini(struct vmod_brotli_decoder **decp)
{
	struct vmod_brotli_decoder *dec;

	if (decp == NULL || *decp == NULL)
		return;
	CHECK_OBJ(*decp, VMOD_BROTLI_DECODER_MAGIC);
	dec = *decp;
	*decp = NULL;
	if (dec->vcl_name != NULL)
		free(dec->vcl_name);
	FREE_OBJ(dec);
}

VCL_VOID
vmod_decoder_create_stats(VRT_CTX, struct vmod_brotli_decoder *dec)
{
	CHECK_OBJ_NOTNULL(dec, VMOD_BROTLI_DECODER_MAGIC);
	create_stats(ctx, dec->vfp, dec->vcl_name);
}

/* Version functions */

static VCL_STRING
brotli_version(VRT_CTX, uint32_t version)
{
	VCL_STRING vers;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	unsigned major = version >> 24;
	unsigned minor = (version >> 12) & 0xFFF;
	unsigned patch = version & 0xFFF;
	if ((vers = WS_Printf(ctx->ws, "%u.%u.%u", major, minor, patch))
	    == NULL) {
		VSLb(ctx->vsl, SLT_VCL_Error, "vfp brotli error: "
		     "insufficient workspace for version string");
		return ("(insufficient workspace for version string)");
	}
	return vers;
}

VCL_STRING
vmod_encoder_version(VRT_CTX)
{
	return (brotli_version(ctx, BrotliEncoderVersion()));
}

VCL_STRING
vmod_decoder_version(VRT_CTX)
{
	return (brotli_version(ctx, BrotliDecoderVersion()));
}

VCL_STRING
vmod_version(VRT_CTX)
{
	(void) ctx;
	return VERSION;
}
