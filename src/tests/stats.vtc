# looks like -*- vcl -*-

varnishtest "statistics"

server s1 {
	loop 10 {
		rxreq
		txresp -body {Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.}
	}
} -start

client c1 {
	loop 10 {
		txreq
		rxresp
		expect resp.status == 200
		expect resp.http.Content-Encoding == <undef>
		expect resp.body == {Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.}
	}
}

# Debug param vclrel causes VCLs to be released without delay. This
# makes it possible to check the effects of temperature changes (stats
# are hidden and revealed as VCLs go cold and warm).
varnish v1 -arg "-p debug=+vclrel" -vcl+backend {
	import ${vmod_brotli};

	sub vcl_backend_response {
		set beresp.filters = "br unbr";
		set beresp.uncacheable = true;
	}
} -start

client c1 -run

varnish v1 -vsc BROTLI.*

varnish v1 -expect BROTLI.br.ops == 10
varnish v1 -expect BROTLI.br.in > 0
varnish v1 -expect BROTLI.br.out > 0
varnish v1 -expect BROTLI.unbr.ops == 10
varnish v1 -expect BROTLI.unbr.in > 0
varnish v1 -expect BROTLI.unbr.out > 0

varnish v1 -vcl+backend {
	import ${vmod_brotli};

	sub vcl_init {
		new mybr = brotli.encoder("mybr");
		mybr.create_stats();
		new myunbr = brotli.decoder("myunbr");
		myunbr.create_stats();
	}

	sub vcl_backend_response {
		set beresp.filters = "mybr myunbr";
		set beresp.uncacheable = true;
	}
}

varnish v1 -cliok "vcl.discard vcl1"
varnish v1 -cliok "vcl.list"

varnish v1 -vsc BROTLI.*
varnish v1 -expect BROTLI.br.ops == 10
varnish v1 -expect BROTLI.br.in > 0
varnish v1 -expect BROTLI.br.out > 0
varnish v1 -expect BROTLI.unbr.ops == 10
varnish v1 -expect BROTLI.unbr.in > 0
varnish v1 -expect BROTLI.unbr.out > 0

server s1 -wait
server s1 -start
client c1 -run

varnish v1 -vsc BROTLI.*
varnish v1 -expect BROTLI.br.ops == 10
varnish v1 -expect BROTLI.br.in > 0
varnish v1 -expect BROTLI.br.out > 0
varnish v1 -expect BROTLI.unbr.ops == 10
varnish v1 -expect BROTLI.unbr.in > 0
varnish v1 -expect BROTLI.unbr.out > 0
varnish v1 -expect BROTLI.vcl2.mybr.ops == 10
varnish v1 -expect BROTLI.vcl2.mybr.in > 0
varnish v1 -expect BROTLI.vcl2.mybr.out > 0
varnish v1 -expect BROTLI.vcl2.myunbr.ops == 10
varnish v1 -expect BROTLI.vcl2.myunbr.in > 0
varnish v1 -expect BROTLI.vcl2.myunbr.out > 0

varnish v1 -vcl+backend {}

varnish v1 -cliok "vcl.state vcl2 cold"

# With vcl2 in the cold state, stats for vcl2.* do not appear.
# This has to be checked manually in the log.
varnish v1 -vsc BROTLI.vcl2.*
varnish v1 -expect BROTLI.br.ops == 10
varnish v1 -expect BROTLI.br.in > 0
varnish v1 -expect BROTLI.br.out > 0
varnish v1 -expect BROTLI.unbr.ops == 10
varnish v1 -expect BROTLI.unbr.in > 0
varnish v1 -expect BROTLI.unbr.out > 0

varnish v1 -cliok "vcl.state vcl2 warm"

# With vcl2 back in the warm state, stats for vcl2.* appear again.
varnish v1 -expect BROTLI.br.ops == 10
varnish v1 -expect BROTLI.br.in > 0
varnish v1 -expect BROTLI.br.out > 0
varnish v1 -expect BROTLI.unbr.ops == 10
varnish v1 -expect BROTLI.unbr.in > 0
varnish v1 -expect BROTLI.unbr.out > 0
varnish v1 -expect BROTLI.vcl2.mybr.ops == 10
varnish v1 -expect BROTLI.vcl2.mybr.in > 0
varnish v1 -expect BROTLI.vcl2.mybr.out > 0
varnish v1 -expect BROTLI.vcl2.myunbr.ops == 10
varnish v1 -expect BROTLI.vcl2.myunbr.in > 0
varnish v1 -expect BROTLI.vcl2.myunbr.out > 0

varnish v1 -cliok "vcl.state vcl2 cold"
varnish v1 -cliok "vcl.discard vcl2"
varnish v1 -cliok "vcl.list"

varnish v1 -vsc BROTLI.vcl2.*
varnish v1 -expect BROTLI.br.ops == 10
varnish v1 -expect BROTLI.br.in > 0
varnish v1 -expect BROTLI.br.out > 0
varnish v1 -expect BROTLI.unbr.ops == 10
varnish v1 -expect BROTLI.unbr.in > 0
varnish v1 -expect BROTLI.unbr.out > 0
