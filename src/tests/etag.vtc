# looks like -*- vcl -*-

varnishtest "Backend IMS and brotli de-/compression"

# cf. Varnish g00006.vtc

server s1 {
	rxreq
	expect req.url == /1
	txresp -hdr  "Last-Modified: Wed, 11 Sep 2013 13:36:55 GMT" \
		-hdr {ETag: "foozle"} \
		-body {The quick brown fox jumps over the lazy dog}

	rxreq
	expect req.url == /1
	expect req.http.if-modified-since == "Wed, 11 Sep 2013 13:36:55 GMT"
	txresp -status 304 \
		-hdr {ETag: "fizle"} \
		-nolen

	rxreq
	expect req.url == /2
	txresp -hdr "Last-Modified: Wed, 11 Sep 2013 13:36:55 GMT" \
		-hdr {ETag: "foobar"} -hdr "Content-Encoding: br" \
		-body {�The quick brown fox jumps over the lazy dog}

	rxreq
	expect req.url == /2
	expect req.http.if-modified-since == "Wed, 11 Sep 2013 13:36:55 GMT"
	txresp -status 304 -hdr "Content-Encoding: br" \
		-hdr {ETag: "snafu"} \
		-nolen

} -start

varnish v1 -vcl+backend {
	import ${vmod_brotli};

	sub vcl_backend_response {
		set beresp.http.foobar = beresp.http.content-encoding;
		if (bereq.url == "/1") {
			set beresp.filters = "br";
		} else {
			set beresp.filters = "unbr";
		}
		set beresp.ttl = 1s;
		set beresp.grace = 0s;
		set beresp.keep = 60s;
	}
} -start

client c1 {
	txreq -url /1 -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.content-encoding == "br"
	expect resp.http.foobar == ""
	expect resp.http.etag == {W/"foozle"}
	expect resp.bodylen == 47
	expect resp.body == "�The quick brown fox jumps over the lazy dog"

	delay 1

	txreq -url /1 -hdr "Accept-Encoding: br"
	rxresp
	expect resp.http.content-encoding == "br"
	expect resp.http.foobar == "br"
	expect resp.http.etag == {W/"fizle"}
	expect resp.bodylen == 47
	expect resp.body == "�The quick brown fox jumps over the lazy dog"

	delay .2

	txreq -url /2
	rxresp
	expect resp.http.content-encoding == <undef>
	expect resp.http.foobar == "br"
	expect resp.http.etag == {W/"foobar"}
	expect resp.bodylen == 43
	expect resp.body == "The quick brown fox jumps over the lazy dog"

	delay 1

	txreq -url /2
	rxresp
	expect resp.http.content-encoding == <undef>
	# Here we see the C-E of the IMS OBJ
	expect resp.http.foobar == ""
	expect resp.http.etag == {W/"snafu"}
	expect resp.bodylen == 43
	expect resp.body == "The quick brown fox jumps over the lazy dog"
} -run

# A beresp is first gzipped by Varnish, then the backend sends an IMS
# response claiming that the validated object is br-encoded. The following
# is to verify that Varnish does the right thing.
server s1 -wait
server s1 {
	rxreq
	txresp -hdr  "Last-Modified: Wed, 11 Sep 2013 13:36:55 GMT" \
		-hdr {ETag: "foo"} \
		-body {The quick brown fox jumps over the lazy dog}

	rxreq
	expect req.http.if-modified-since == "Wed, 11 Sep 2013 13:36:55 GMT"
	txresp -status 304 \
		-hdr {ETag: "bar"} -hdr "Content-Encoding: br" \
		-nolen
} -start

varnish v1 -vcl+backend {
	sub vcl_backend_response {
		set beresp.http.foobar = beresp.http.content-encoding;
		set beresp.do_gzip = true;
		set beresp.ttl = 0.5s;
		set beresp.grace = 0s;
		set beresp.keep = 60s;
	}
}

client c1 {
	txreq -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.content-encoding == "gzip"
	expect resp.http.foobar == ""
	expect resp.http.etag == {W/"foo"}
	gunzip
	expect resp.body == "The quick brown fox jumps over the lazy dog"
} -run

delay 0.6

# Although br compression is specified here, it is bypassed for the
# 304 backend response.
varnish v1 -vcl+backend {
	import ${vmod_brotli};

	sub vcl_backend_response {
		set beresp.http.foobar = beresp.http.content-encoding;
		set beresp.filters = "unbr";
	}
}

# A client that does not send Accept-Encoding gets the decompressed
# object, with no Content-Encoding header. Header foobar shows the
# Content-Encoding header as saved in the cached object, which is
# still "gzip".
client c1 {
	txreq
	rxresp
	expect resp.http.content-encoding == <undef>
	expect resp.http.foobar == "gzip"
	expect resp.http.etag == {W/"bar"}
	expect resp.body == "The quick brown fox jumps over the lazy dog"
} -run
