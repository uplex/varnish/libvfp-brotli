INSTALLATION
============

Installing prerequisites
~~~~~~~~~~~~~~~~~~~~~~~~

The VFP requires the Google brotli library, which is installed as
separate libraries for compression and decompression,
``libbrotlienc.so`` and ``libbrotlidec.so``. These in turn depend on a
third library ``libbrotlicommon.so``. Package managers for most
distributions provide these under a name like ``libbrotli`` or
``libbrotli1`` (and the package manager takes care of the dependencies
for you).

For deployment with Varnish, the non-development version of libbrotli
is sufficient, while the development version is necessary for source
builds (see below).

You can also build libbrotli from the source repository at
https://github.com/google/brotli.git

Building from source
~~~~~~~~~~~~~~~~~~~~

Source builds require the development version of libbrotli, which may
be available as a package for your distribution under a name like
``libbrotli-dev`` or ``libbrotli-devel``. An install from the
libbrotli source is also sufficient.

The VFP is built against a Varnish installation, and the autotools use
``pkg-config(1)`` to locate the necessary header files and other
resources for both Varnish and libbrotli. This sequence will install
the VMOD::

  > ./autogen.sh	# for builds from the git repo
  > ./configure
  > make
  > make check		# to run unit tests in src/tests/*.vtc
  > make distcheck	# run check and prepare a distribution tarball
  > sudo make install

See `CONTRIBUTING.rst <CONTRIBUTING.rst>`_ for notes about building
from source.

If you have installed Varnish or libbrotli in non-standard
directories, call ``autogen.sh`` and ``configure`` with the
``PKG_CONFIG_PATH`` environment variable set to include the paths
where the ``.pc`` file can be located for all of ``varnishapi``,
``libbrotlienc``, ``libbrotlidec`` and ``libbrotlicommon``. For
example, when varnishd configure was called with ``--prefix=$PREFIX``,
use::

  > PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  > export PKG_CONFIG_PATH

By default, the ``configure`` script installs the VFP in the same
directory as Varnish, determined via ``pkg-config(1)``. The vmod
installation directory can be overridden by passing the ``VMOD_DIR``
variable to ``configure``.

Other files such as the man-page are installed in the locations
determined by ``configure``, which inherits its default ``--prefix``
setting from Varnish.
